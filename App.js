import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {Provider} from 'react-redux';
import Route from './Route';
import Store from './src/Redux/Store';

export default function App() {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Route />
      </NavigationContainer>
    </Provider>
  );
}
