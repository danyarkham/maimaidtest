import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import List from './src/Screen/List';
import Update from './src/Screen/Update';
import Create from './src/Screen/Create';

const Stack = createNativeStackNavigator();

export default function Route() {
  return (
    <Stack.Navigator initialRouteName="List">
      <Stack.Screen
        name="List"
        component={List}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Update"
        component={Update}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Create"
        component={Create}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}
