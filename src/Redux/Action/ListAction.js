export const GET_LIST_DATA = 'GET_LIST_DATA';
export const SET_LIST_DATA = 'SET_LIST_DATA';
export const UPDATE_DATA = 'UPDATE_DATA';
export const CREATE_DATA = 'CREATE_DATA';

export const getListData = payload => {
  return {
    type: GET_LIST_DATA,
    payload,
  };
};

export const setListData = payload => {
  return {
    type: SET_LIST_DATA,
    payload,
  };
};

export const updateData = payload => {
  return {
    type: UPDATE_DATA,
    payload,
  };
};

export const createData = payload => {
  return {
    type: CREATE_DATA,
    payload,
  };
};
