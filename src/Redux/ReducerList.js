import {GET_LIST_DATA, SET_LIST_DATA} from './Action/ListAction';

const initialState = {
  data: [],
};

const ReducerList = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST_DATA:
      return {
        ...state,
        data: action.payload,
      };

    default:
      return state;
  }
};

export default ReducerList;
