import {takeLatest} from '@redux-saga/core/effects';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {CREATE_DATA} from '../Action/ListAction';

function* getSagaCreate(action) {
  console.log(action.payload);
  try {
    const result = yield axios({
      method: 'post',
      url: 'http://maimaid.id:8002/user/create',
      data: action.payload,
    });
    console.log(result.data, 'SAGA CREATE');
    if (result.data.status.code === 200) {
      //   yield put(setListData(result.data.data));
      Snackbar.show({
        text: result.data.status.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#27ae60',
      });
    }
    if (result.data.status.code === 401) {
      //   yield put(setListData(result.data.data));
      Snackbar.show({
        text: result.data.status.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#27ae60',
      });
    }
  } catch (e) {
    console.log(e);
  }
}

function* SagaCreate() {
  yield takeLatest(CREATE_DATA, getSagaCreate);
}

export default SagaCreate;
