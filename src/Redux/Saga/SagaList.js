import {put, takeLatest, takeEvery} from '@redux-saga/core/effects';
import axios from 'axios';
import {GET_LIST_DATA, setListData} from '../Action/ListAction';

function* getSagaList(action) {
  try {
    const result = yield axios({
      method: 'post',
      url: 'http://maimaid.id:8002/user/read',
      // data: action.payload,
    });
    console.log(result.data.data, 'GET LIST');
    if (result.data.status.code === 200) {
      yield put(setListData(result.data.data));
    }
  } catch (e) {
    console.log(e);
  }
}

function* SagaList() {
  yield takeEvery(GET_LIST_DATA, getSagaList);
}

export default SagaList;
