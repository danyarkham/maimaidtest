import {put, takeLatest} from '@redux-saga/core/effects';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {setListData, UPDATE_DATA} from '../Action/ListAction';

function* getSagaUpdate(action) {
  try {
    const result = yield axios({
      method: 'post',
      url: 'http://maimaid.id:8002/user/update',
      data: action.payload,
    });
    console.log(result.data, 'SAGA UPDATE');
    if (result.data.status.code === 200) {
      //   yield put(setListData(result.data.data));
      Snackbar.show({
        text: result.data.status.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#27ae60',
      });
    }
    if (result.data.status.code === 401) {
      //   yield put(setListData(result.data.data));
      Snackbar.show({
        text: result.data.status.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#27ae60',
      });
    }
  } catch (e) {
    console.log(e);
  }
}

function* SagaUpdate() {
  yield takeLatest(UPDATE_DATA, getSagaUpdate);
}

export default SagaUpdate;
