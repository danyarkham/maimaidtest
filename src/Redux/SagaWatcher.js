import {all} from 'redux-saga/effects';
import SagaCreate from './Saga/SagaCreate';
import SagaList from './Saga/SagaList';
import SagaUpdate from './Saga/SagaUpdate';

export function* SagaWatcher() {
  yield all([SagaList(), SagaCreate(), SagaUpdate()]);
}
