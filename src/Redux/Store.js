import {applyMiddleware, createStore} from 'redux';
import {AllReducer} from './AllReducer';
import createSagaMiddleware from 'redux-saga';
import {SagaWatcher} from './SagaWatcher';
import logger from 'redux-logger';

const sagaMiddleware = createSagaMiddleware();
const Store = createStore(AllReducer, applyMiddleware(sagaMiddleware, logger));

export default Store;

sagaMiddleware.run(SagaWatcher);
