import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {useDispatch} from 'react-redux';
import {createData} from '../Redux/Action/ListAction';

export default function Create({navigation}) {
  const [fullname, setFullname] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [gender, setGender] = useState(null);
  const genderSwitcher = gender == 'pria' ? 1 : 2;
  console.log(genderSwitcher);
  const [dob, setDob] = useState(null);
  console.log(fullname, email, password, genderSwitcher);

  const dispatch = useDispatch();
  const CreateData = () => {
    dispatch(
      createData({fullname, email, password, gender: genderSwitcher, dob}),
    );
    navigation.navigate('List');
  };

  return (
    <View style={styles.container}>
      <View style={styles.editContainer}>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 20,
            color: 'rgba(0,0,0,0.7)',
            marginBottom: heightPercentageToDP(2),
          }}>
          Add Data
        </Text>
        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            placeholder="Fullname"
            onChangeText={e => setFullname(e)}
          />
        </View>

        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput placeholder="Email" onChangeText={e => setEmail(e)} />
        </View>

        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            placeholder="Password"
            onChangeText={e => setPassword(e)}
            secureTextEntry={true}
          />
        </View>

        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            placeholder="Gender"
            onChangeText={e => setGender(e.toLocaleLowerCase())}
          />
        </View>

        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            placeholder='Date of birth "YYYY-MM-DD"'
            onChangeText={e => setDob(e)}
          />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={CreateData}
            activeOpacity={0.7}
            style={{
              height: heightPercentageToDP(8),
              backgroundColor: 'rgba(75,200,75,1)',
              borderRadius: moderateScale(10),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: 'white', fontWeight: '300', fontSize: 20}}>
              Submit Data
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  editContainer: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: moderateScale(5),
    paddingHorizontal: widthPercentageToDP(5),
    paddingVertical: heightPercentageToDP(2),
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: heightPercentageToDP(2),
    marginTop: heightPercentageToDP(2),
  },
});
