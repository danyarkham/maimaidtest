import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  LayoutAnimation,
  LogBox,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {useDispatch, useSelector} from 'react-redux';
import {getListData} from '../Redux/Action/ListAction';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import * as Animatable from 'react-native-animatable';

export const ProfileCard = ({
  onPress,
  fullname,
  gender,
  email,
  dob,
  elevation = 100,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      style={{
        paddingHorizontal: widthPercentageToDP(2),
        paddingVertical: heightPercentageToDP(5),
        backgroundColor: 'white',
        borderColor: '#FAFAFA',
        borderWidth: 1,
        borderRadius: moderateScale(10),
        marginBottom: heightPercentageToDP(2),
        alignItems: 'center',
        elevation,
      }}>
      <Text style={{color: 'black', fontWeight: '500'}}>{fullname}</Text>
      <View
        style={{
          width: '15%',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={{color: 'black', fontWeight: '300'}}>
          {gender === 1 ? 'Pria' : 'Wanita'}
        </Text>
        <MaterialCommunityIcons
          name={gender === 1 ? 'gender-male' : 'gender-female'}
          color="black"
          size={20}
        />
      </View>
      <Text style={{color: 'black', fontWeight: '300'}}>{email}</Text>
      <Text style={{color: 'black', fontWeight: '300'}}>{dob}</Text>
    </TouchableOpacity>
  );
};

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

export default function List({navigation, route}) {
  const [isLoading, setIsLoading] = useState(true);

  const {data} = useSelector(state => state.ReducerList) || {};

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getListData({page: 1, offset: 5}));
    LogBox.ignoreLogs(['Animated: `useNativeDriver`', 'componentWill']);
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, [isLoading]);

  const [showButton, setShowButton] = useState(true);

  const scrollOffset = useRef(0);

  const handleScroll = useCallback(
    (event: NativeSyntheticEvent<NativeScrollEvent>) => {
      const CustomLayoutLinear = {
        duration: 200,
        create: {
          type: LayoutAnimation.Types.linear,
          property: LayoutAnimation.Properties.opacity,
        },
        update: {
          type: LayoutAnimation.Types.linear,
          property: LayoutAnimation.Properties.opacity,
        },
        delete: {
          type: LayoutAnimation.Types.linear,
          property: LayoutAnimation.Properties.scaleY,
        },
      };
      // Check if the user is scrolling up or down by confronting the new scroll position with your own one
      const currentOffset = event.nativeEvent.contentOffset.y;
      const direction =
        currentOffset > 0 && currentOffset > scrollOffset.current
          ? 'down'
          : 'up';
      // If the user is scrolling down (and the action-button is still visible) hide it
      const isActionButtonVisible = direction === 'up';
      if (isActionButtonVisible !== showButton) {
        LayoutAnimation.configureNext(CustomLayoutLinear);
        setShowButton(isActionButtonVisible);
      }
      // Update your scroll position
      scrollOffset.current = currentOffset;
    },
    [showButton],
    console.log(showButton, 'showbutton'),
  );

  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    dispatch(getListData({page: 1, offset: 5}));
    wait(1000).then(() => setRefreshing(false));
  }, []);

  return (
    <View style={styles.container}>
      {isLoading && (
        <View
          style={{
            zIndex: 1000,
            position: 'absolute',
            width: '100%',
            top: heightPercentageToDP(50),
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" color="black" />
        </View>
      )}
      {showButton && (
        <Animatable.View
          animation="bounce"
          style={{
            zIndex: 1000,
            position: 'absolute',
            width: '100%',
            bottom: heightPercentageToDP(20),
            right: heightPercentageToDP(1),
          }}>
          <ActionButton buttonColor="rgba(231,76,60,1)">
            <ActionButton.Item
              buttonColor="rgba(75,200,75,1)"
              title="Add People Data"
              onPress={() => navigation.navigate('Create')}>
              <Icon name="md-create" color="white" size={20} />
            </ActionButton.Item>
          </ActionButton>
        </Animatable.View>
      )}
      <ScrollView
        refreshControl={
          <RefreshControl
            colors={['#E2494B']}
            style={{zIndex: 100}}
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
        scrollEventThrottle={16}
        onScroll={handleScroll}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollContainer}>
        {data?.rows?.length > 0
          ? data?.rows.map((e, i) => {
              return (
                <ProfileCard
                  onPress={() => navigation.navigate('Update', e)}
                  key={i}
                  fullname={e.fullname}
                  gender={e.gender}
                  email={e.email}
                  dob={e.dob === null ? 'You never been born' : e.dob}
                />
              );
            })
          : null}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(231,76,60,1)',
  },
  scrollContainer: {
    paddingTop: heightPercentageToDP(2),
    paddingHorizontal: widthPercentageToDP(6),
  },
});
