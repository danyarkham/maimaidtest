import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {useDispatch} from 'react-redux';
import {updateData} from '../Redux/Action/ListAction';
import {ProfileCard} from './List';

export default function Update({navigation, route}) {
  const id = route?.params?.id;
  const [fullname, setFullname] = useState(route?.params?.fullname);
  const [email, setEmail] = useState(route?.params?.email);
  const [password, setPassword] = useState(null);
  const [gender, setGender] = useState(route?.params?.gender);
  const genderSwitcher = gender == 'pria' ? 1 : 2;
  const [dob, setDob] = useState(null);

  console.log(id, fullname, email, password, genderSwitcher, dob);
  console.log(route.params);
  const item = route?.params;

  const dispatch = useDispatch();

  const SubmitChange = () => {
    dispatch(
      updateData({
        id,
        fullname,
        email,
        password,
        gender: genderSwitcher,
        dob,
      }),
    );
    navigation.navigate('List');
  };

  return (
    <View style={styles.container}>
      <ProfileCard
        email={item.email}
        fullname={item.fullname}
        gender={item.gender}
        dob={item.dob === null ? 'You never been born' : item.dob}
        elevation={null}
      />
      <View style={styles.editContainer}>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 20,
            color: 'rgba(0,0,0,0.7)',
            marginBottom: heightPercentageToDP(2),
          }}>
          Edit Data
        </Text>
        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            defaultValue={item.fullname}
            onChangeText={e => setFullname(e)}
          />
        </View>

        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            defaultValue={item.email}
            onChangeText={e => setEmail(e)}
          />
        </View>

        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            placeholder="Password"
            onChangeText={e => setPassword(e)}
            secureTextEntry={true}
          />
        </View>

        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            defaultValue={item.gender === 1 ? 'Pria' : 'Wanita'}
            value={gender}
            onChangeText={e => setGender(e.toLocaleLowerCase())}
          />
        </View>

        <View
          style={{
            borderColor: '#EAEAEA',
            borderWidth: 1,
            borderRadius: moderateScale(5),
            paddingHorizontal: widthPercentageToDP(2),
            marginBottom: heightPercentageToDP(1),
          }}>
          <TextInput
            placeholder={'YYYY-MM-DD'}
            onChangeText={e => setDob(e)}
            value={dob}
          />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={SubmitChange}
            activeOpacity={0.7}
            style={{
              height: heightPercentageToDP(6),
              backgroundColor: 'rgba(75,200,75,1)',
              borderRadius: moderateScale(10),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: 'white', fontWeight: '300', fontSize: 16}}>
              Submit Change
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(231,76,60,1)',
    paddingHorizontal: widthPercentageToDP(6),
    paddingVertical: heightPercentageToDP(2),
  },
  editContainer: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: moderateScale(5),
    paddingHorizontal: widthPercentageToDP(5),
    paddingVertical: heightPercentageToDP(2),
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: heightPercentageToDP(2),
  },
});
